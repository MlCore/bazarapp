﻿using Xunit;
using BazarApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BazarApp.Models.Tests
{
    public class PointOfSellServiceTests
    {
      
        [Fact()]
        public void WriteHeaderTest()
        {
            //arrange
            PointOfSellService service = new PointOfSellService();

            //act
            bool exist = File.Exists(service.OutputFilename);

            //assert
            Assert.True(exist, @"$Output File does not exist");
        }

        [Fact()]
        public void SellCompleteTest()
        {
            //arrange
            PointOfSellService service = new PointOfSellService();
            var list = new ObservableCollection<SellItem>();

            //act
            for (int i = 0; i < 10; i++)
            {
                var item = new SellItem(i, i);
                list.Add(item);
            }
            service.SellComplete(list);

            //assert

            bool exist = File.Exists(service.OutputFilename);
            Assert.True(exist, @"$Output File does not exist");
        }

        [Fact()]
        public void SellCompletePerformanceTest()
        {
            //arrange
            PointOfSellService service = new PointOfSellService();
            var list = new ObservableCollection<SellItem>();

            //act
            for (int customerIndex = 0; customerIndex < 50; customerIndex++)
            {
                for (int i = 0; i < 200; i++)
                {
                    var item = new SellItem(i, i);
                    list.Add(item);
                }
                service.SellComplete(list);
            }
            

            //assert

            bool exist = File.Exists(service.OutputFilename);
            Assert.True(exist, @"$Output File does not exist");
        }


    }
}