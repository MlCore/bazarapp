﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using NLog;

namespace BazarApp.Models
{

    public class SellItem
    {
        
        public int SellerNo { get; set; }

        public decimal Price {get; set;}

        public DateTime TransactionTime { get; set; }

        public SellItem()
        {
            SellerNo = 0;
            Price = 0;
            TransactionTime = DateTime.Now;
        }

        public SellItem(int sellerNo, decimal price)
        {
            SellerNo = sellerNo;
            Price = price;
            TransactionTime = DateTime.Now;
        }

        public override string ToString()
        {
            return $@"Verkäufer: {SellerNo}     Preis: {Price} €";
        }
    }

    public class PointOfSellService : IPointOfSellService
    {
        private static readonly Logger Logger_ = LogManager.GetCurrentClassLogger();
        public ObservableCollection<SellItem> AllItemsList { get; private set; }

        public string OutputFilename { get; }

        public PointOfSellService()
        {
            Logger_.Debug($@"POS Service Started");
            AllItemsList = new ObservableCollection<SellItem>();
            OutputFilename = $@".\Bill_Output.csv";
            WriteHeader();
        }

        public void SellComplete(ObservableCollection<SellItem> list)
        {
            AllItemsList.AddRange(list);
            SaveUpdate(list);
           
        }

        public void WriteHeader()
        {
            //check output file exist
            if (!File.Exists(OutputFilename))
            {
                var swr = new StreamWriter(OutputFilename, true);
                var csv = new CsvWriter(swr);
                csv.Configuration.CultureInfo = CultureInfo.InvariantCulture;

                try
                {
                    csv.WriteRecords(new ObservableCollection<SellItem>());
                }
                catch (Exception e)
                {
                    Logger_.Error($@"Writing CSV header fails, {e.Message}");
                }
                finally
                {
                    swr?.Dispose();
                }

            }
        }

        public void SaveUpdate(ObservableCollection<SellItem> list)
        {
           
            var swr = new StreamWriter(OutputFilename,true);
            var csv = new CsvWriter(swr);
            csv.Configuration.CultureInfo = CultureInfo.InvariantCulture;
            csv.Configuration.HasHeaderRecord = false;

            try
            {
                csv.WriteRecords(list);
            }
            catch (Exception e)
            {
                Logger_.Error($@"Writing CSV data fails, {e.Message}"); ;
            }
            finally
            {
                swr?.Dispose();
            }
        }
    }
}
