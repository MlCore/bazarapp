﻿using System.Collections.ObjectModel;

namespace BazarApp.Models
{
    public interface IPointOfSellService
    {
        ObservableCollection<SellItem> AllItemsList { get; }
        void SellComplete(ObservableCollection<SellItem> list);
        void SaveUpdate(ObservableCollection<SellItem> list);
    }
}