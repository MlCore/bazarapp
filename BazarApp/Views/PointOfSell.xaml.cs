﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BazarApp.Views
{
    /// <summary>
    /// Interaction logic for PointOfSell.xaml
    /// </summary>
    public partial class PointOfSell : UserControl
    {
        public PointOfSell()
        {
            InitializeComponent();
        }

        private void SellerId_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Price.Focus();
            }
        }

        private void Price_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SellerId.Focus();
            }
        }
    }
}
