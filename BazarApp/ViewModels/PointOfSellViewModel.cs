﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BazarApp.Models;
using NLog;
using Prism.Commands;
using Prism.Mvvm;

namespace BazarApp.ViewModels
{
    public class PointOfSellViewModel: BindableBase
    {
        private static readonly Logger Logger_ = LogManager.GetCurrentClassLogger();

        public DelegateCommand ScanItemCommand { get; set; }
        public DelegateCommand NextCustomerCommand { get; set; }
        public DelegateCommand DeleteItemCommand { get; set; }
        public DelegateCommand DeleteAllCommand { get; set; }

        private IPointOfSellService service;
        //private NumberStyles style;
        
        private ObservableCollection<SellItem> _customerItems;
        public ObservableCollection<SellItem> CustomerItems
        {
            get { return _customerItems; }
            set { SetProperty(ref _customerItems, value); }
        }

        public SellItem SelectedSellItem { get; set; }

        private string _sellerNo;
        public string SellerNo
        {
            get { return _sellerNo; }
            set
            {
                string txt = value;
                if (txt != "")
                {
                    txt = _charsToDestroyForIdNumber.Replace(txt, "");   
                }

                if (txt.Length == 3)
                {
                    SellerNoValid = true;
                }
                SetProperty(ref _sellerNo, txt);
            }
        }
        private bool _sellerNoValid;
        public bool SellerNoValid
        {
            get => _sellerNoValid;
            set { SetProperty(ref _sellerNoValid, value); }
        }

        private string _price;
        public string Price
        {
            get { return _price; }
            set
            {
                string txt = value;
                if (txt != "")
                {
                    txt = txt.Replace(",", ".");
                    txt = _charsToDestroyForDecimal.Replace(txt, "");
                }
                SetProperty(ref _price, txt);
            }
        }

        private string _received;

        public string Received
        {
            get { return _received; }
            set
            {
                string txt = value;
                if (txt != "")
                {
                    txt = txt.Replace(",", ".");
                    txt = _charsToDestroyForDecimal.Replace(txt, "");
                }
                SetProperty(ref _received, txt);
                UpdateCashToReturn();
                
            }
        }
       
        private string _toReturn;

        public string ToReturn
        {
            get { return _toReturn; }
            set { SetProperty(ref _toReturn, value); }
        }

        private string _sum;
        private Regex _charsToDestroyForDecimal;
        private Regex _charsToDestroyForIdNumber;

        public string Sum
        {
            get { return _sum; }
            set { SetProperty(ref _sum, value); }
        }

        private string _count;
        
        public string Count
        {
            get { return _count; }
            set { SetProperty(ref _count, value); }
        }

        public PointOfSellViewModel(IPointOfSellService s)
        {
            service = s;
            //style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite;
            CustomerItems = new ObservableCollection<SellItem>();
            
            ScanItemCommand = new DelegateCommand(ScanCommandHandler);
            NextCustomerCommand = new DelegateCommand(NextCustomerCommandHandler);
            DeleteAllCommand = new DelegateCommand(DeleteAllCommandHandler);
            DeleteItemCommand = new DelegateCommand(DeleteItemCommandHandler);
            DeleteAllCommandHandler();
            _charsToDestroyForDecimal = new Regex(@"[^\d|\.]");
            _charsToDestroyForIdNumber = new Regex("[^0-9]");
            SellerNoValid = true;
        }

        private void DeleteItemCommandHandler()
        {
            if (SelectedSellItem != null)
            {
                this.CustomerItems.Remove(SelectedSellItem);
                UpdateSum();
                UpdateCashToReturn();
            }
        }

        private void DeleteAllCommandHandler()
        {
            CustomerItems.Clear();
            Price = "";
            SellerNo = "";
            Sum = "";
            Received = "";
            Logger_.Debug($"All Items deleted");
        }

        private void NextCustomerCommandHandler()
        {
            service.SellComplete(CustomerItems);
            CustomerItems.Clear();
            Price = "";
            SellerNo = "";
            Sum = "";
            Received = "";
            Count = "";
            SellerNoValid = true;
        }

        private void ScanCommandHandler()
        {
            try
            {
                bool notEmpty = int.TryParse(SellerNo, out int no);

                if (notEmpty && no > 99 && no < 1000)
                {
                    SellerNoValid = true;
                    decimal price = decimal.Parse(Price, CultureInfo.InvariantCulture);
                    var x = new SellItem(no, price);
                    CustomerItems.Add(x);

                    UpdateSum();
                    UpdateCashToReturn();
                    Price = "";
                    SellerNo = "";
                }
                else
                {
                    SellerNoValid = false;
                }

                
            }
            catch (Exception e)
            {
                Logger_.Error($"Enter Article fails, {e.Message}");
            }
        }

        private void UpdateSum()
        {
            decimal sum = 0;
            foreach (var item in CustomerItems)
            {
                sum += item.Price;
            }
            Sum = sum.ToString("F2", CultureInfo.InvariantCulture);

            if (CustomerItems.Count > 0)
            {
                Count = $"({CustomerItems.Count})";
            }
            else
            {
                Count = "";
            }
        }

        public void UpdateCashToReturn()
        {
            try
            {
                decimal rec = decimal.Parse(Received, CultureInfo.InvariantCulture);
                decimal sum = decimal.Parse(Sum, CultureInfo.InvariantCulture);
                decimal diff = rec - sum;
                ToReturn = diff.ToString("F2", CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                ToReturn = "";
            }
        }
    }
}
